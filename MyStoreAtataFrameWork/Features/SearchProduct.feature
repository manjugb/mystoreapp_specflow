﻿@MyStore
Feature: Search Product
@MyStoreSearch
Scenario: MyStore Page As a User Able to search product
Given I am on Login page
When I type <productName> in the Search Tab
Then I Click on Search Button
Then I Validate <ResultsText> Results Displayed

Examples:
|productName|ResultsText|
|Blouse|1 result has been found.|
|Printed Dress|5 results have been found.|
|Printed|5 results have been found.|
|Dress|7 results have been found.|
|ddfdg|0 results have been found.|
|""|0 results have been found.|
