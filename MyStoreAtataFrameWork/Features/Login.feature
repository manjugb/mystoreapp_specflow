﻿@MyStoreLogin
Feature: MyStore Login and Search Product
@MyStoreLogin
Scenario: MyStore Page As a User Able to login to Search Product
Given I am on Login page
When I press SignIn
Then I type <user> and <psword> to the form
When I press Login
When I type <productName> in the Search Tab
Then I Click on Search Button
Then I Validate <ResultsText> Results Displayed

Examples:
|user|psword|productName|ResultsText|
|manjugbmsc@hotmail.com|Test@1234|Blouse|1 result has been found.|
|manjugbmsc@hotmail.com|Test@1234|Printed Dress|5 results have been found.|
|manjugbmsc@hotmail.com|Test@1234|Printed|5 results have been found.|
|manjugbmsc@hotmail.com|Test@1234|Dress|7 results have been found.|
|manjugbmsc@hotmail.com|Test@1234|ddfdg|0 results have been found.|
|manjugbmsc@hotmail.com|Test@1234|""|0 results have been found.|