﻿using Atata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStoreAtataFrameWork.Components
{
    using _ = SearchPage;
    public class SearchPage : BasePage<_>
    {
        [FindByXPath("//input[@id='search_query_top']")]
        public TextInput<_> SearchKeyTextBox { get; private set; }

        [FindByXPath("//form[@id='searchbox']//button[@type='submit']")]
        public Button<_> SearchSubmitButton { get; private set; }
        //Displayed Products Results
        [FindByXPath("//span[@class='heading-counter']")]
        public Text<_> ProductResult { get; private set; }

      
    }
}
