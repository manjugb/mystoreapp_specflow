﻿using Atata;

namespace MyStoreAtataFrameWork
{
    using _ = LoginPage;



    public class LoginPage : BasePage<_>
    {

        [FindByXPath("//a[@class='login']")]
        public Link<_> SignIn { get; private set; }

        [FindByName("email")]
        public TextInput<_> Email { get; private set; }

        [FindByName("passwd")]
        public PasswordInput<_> Password { get; private set; }

        [FindByLabel("Login with SSO")]
        public CheckBox<_> loginSSO { get; private set; }

        [FindByCss("#SubmitLogin")]
        public Button<_> Login { get; private set; }

        [FindByXPath("v[1]/div[2]/form[1]/div[1]/p[2]/button[1]/span[1]")]
        public Text<_> ErrorMsg { get; private set; }



    }
  }
