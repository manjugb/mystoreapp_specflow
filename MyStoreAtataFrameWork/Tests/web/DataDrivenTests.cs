﻿using Atata;
using AtataSamples.CsvDataSource;
using MyStoreAtataFrameWork.Models;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;

namespace MyStoreAtataFrameWork
{
    [AllureNUnit]
    [AllureSuite("Login")]
    [AllureOwner("Manjunath")]
    class DataDrivenTests : UITestFixture
    {
        public static TestCaseData[] LoginModels => CsvSource.Get<LoginModel>("C:\\Users\\manju\\source\\repos\\MyStoreAtataFrameWork\\MyStoreAtataFrameWork\\Data\\login-models.csv");

        [Test(Description = "Able to login MyStore Using Valid Credentials")]
        [AllureTag("VS2019")]
        [AllureSubSuite("Login Test using Nunit")]
        [TestCaseSource(nameof(LoginModels))]
        public void LoginWithDifferentCreds(LoginModel model)
        {
            Go.To<LoginPage>()
                 .SignIn.Click()
                 .Email.Set(model.Login)
                 .Password.Set(model.Password.Trim())
                 .Login.Click();
        }
    }
}
