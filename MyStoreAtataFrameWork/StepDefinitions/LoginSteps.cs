﻿using Atata;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using TechTalk.SpecFlow;


namespace MyStoreAtataFrameWork.StepDefinitions
{

    
    [Binding]
    [AllureNUnit]
    [AllureFeature("MyStoreLogin")]
    [AllureOwner("Manjunath")]
    [AllureSuite("LoginTest")]
    
    public class LoginSteps : BaseSteps
    {

        [Given("I am on Login page")]
        public void GivenIAmOnLoginPage()
        {
            Go.To<LoginPage>();
        }

        [Then("I type (.*) and (.*) to the form")]
        public void GivenITypeAndrii_HnatyshynAndAdrenalinToTheForm(string login, string password)
        {
            On<LoginPage>()
                .Email.Set(login)
                .Password.Set(password);
        }



        [When("I press SignIn")]
        public void WhenIPressSignIn()
        {
            On<LoginPage>().SignIn.Click();
        }



        [When("I press Login")]
        public void WhenIPressLogin()
        {
            On<LoginPage>().Login.Click();
        }





        [Then("I Check  Error Messages (.*) to the form")]
        public void ThenIVerifyEmailError(string emailmsglable)
        {
            On<LoginPage>().ErrorMsg.Value.Equals(emailmsglable);
            On<LoginPage>().PageSource.Value.Equals(emailmsglable);


            // Assert.AreEqual(actmsg, emailmsglable, "Strings are not matching");
        }




        [When("I press Password Text Box")]
        public void WhenIPressPasswrdTextBox()
        {
            On<LoginPage>()
           .Password.Click();
        }
    }

}
