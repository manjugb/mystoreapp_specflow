﻿using MyStoreAtataFrameWork.Components;
using NUnit.Allure.Attributes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace MyStoreAtataFrameWork.StepDefinitions
{
    [Binding]
    [AllureOwner("Manjunath")]
    [AllureSuite("ProductSearchTest")]
    public class SearchProductSteps : BaseSteps
    {
        [When("I type (.*) in the Search Tab")]
        public void GivenITypeAndEnterProductSearchTab(string searchkey)
        {
            On<SearchPage>()
                .SearchKeyTextBox.Set(searchkey)
                .SearchSubmitButton.Click();
        }

        [Then("I Click on Search Button")]
        public void ThenIClickOnSearchButton()
        {
            On<SearchPage>().SearchSubmitButton.Click();
        }


        [Then("I Validate (.*) Results Displayed")]
        public void ThenValidateProductResults(string resultstext)
        {
            string acttext = On<SearchPage>().PageSource;
            if (acttext.Contains(acttext))
            {
                StringAssert.Contains(resultstext, acttext);
            }
            else
            {
                StringAssert.DoesNotContain(resultstext, acttext);
            }
            


        }
    }
}
